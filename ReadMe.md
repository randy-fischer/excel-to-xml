# Excel-to-XML, part of the excel to MODS transformation project.

A stand-alone Java app written by Jonathon Pitts, I added junit tests, removed some deprecations.

The program parses an excel spreadsheet and transforms it into more malleable XML, which is used by other parts of
the transformation project.

### Build instructions:

````
mvn clean package
````

### Run as

````
 java -jar target/excelXML-1.0-SNAPSHOT.jar  input-excel-file output-xml-file
````

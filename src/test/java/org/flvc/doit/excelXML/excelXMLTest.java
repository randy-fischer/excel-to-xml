package org.flvc.doit.excelXML;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.*;
import java.nio.charset.StandardCharsets;

import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class excelXMLTest {
    public static File[] inputXLSX;
    public static File expectedXmlDirectory = new File("src/test/resources/xml-expected-output-files/");

    @BeforeClass
    public static void listTextFiles() {
        // one-time initialization code

        File inputXLSXdirectory = new File("src/test/resources/xlsx-input-files/");
        inputXLSX = inputXLSXdirectory.listFiles();
    }


    @Test
    public void ParseExcelTestFiles() throws java.io.IOException {
        String[] args = new String[2];
        excelXML transformer = new excelXML();
        File xmlExpected, xmlGenerated;
        String xmlFilename;

        for (int i = 0; i < inputXLSX.length; i++) {
            if (inputXLSX[i].isFile()) {
                xmlFilename = inputXLSX[i].getName().replace(".xlsx", ".xml");

                xmlGenerated = new File("target/test-classes/xml-generated-output-files/" + xmlFilename);
                xmlExpected = new File(expectedXmlDirectory.getAbsolutePath() + "/" + xmlFilename);

                // System.out.println("input xlst file: " + inputXLSX[i].getAbsolutePath());
                // System.out.println("generated xml file: " + xmlGenerated.getAbsolutePath());
                // System.out.println("expected xml file: " + xmlExpected.getAbsolutePath());

                args[0] = inputXLSX[i].getAbsolutePath();
                args[1] = xmlGenerated.getAbsolutePath();

                excelXML.main(args);

                assertEquals(FileUtils.readLines(xmlGenerated, StandardCharsets.UTF_8),
                             FileUtils.readLines(xmlExpected, StandardCharsets.UTF_8));
            }
        }
    }

}

// import junit.framework.Test;
// import junit.framework.TestCase;
// import junit.framework.TestSuite;
//
// /**
//  * Unit test for simple App.
//  */
// public class excelXMLTest
//     extends TestCase
// {
//
//     public void test1() {
//         // fail("not yet implemented");
//     }
//
//     public void test2() {
//         // fail("not yet implemented");
//     }
//
//     /**
//      * Create the test case
//      *
//      * @param testName name of the test case
//      */
//     public excelXMLTest( String testName )
//     {
//         super( testName );
//     }
//
//     /**
//      * @return the suite of tests being tested
//      */
//     public static Test suite()
//     {
//         return new TestSuite( excelXMLTest.class );
//     }
//
//     /**
//      * Rigourous Test :-)
//      */
//     public void testexcelXML()
//     {
//         assertTrue( true );
//     }
// }
